from pocketsphinx import LiveSpeech, get_model_path
import os
for phrase in LiveSpeech(): print(phrase)

model_path = get_model_path()

speech = LiveSpeech(
    verbose=False,
    sampling_rate=16000,
    buffer_size=2048,
    no_search=False,
    full_utt=False,
    hmm=os.path.join(model_path, 'en-us'),
    lm=os.path.join(model_path, '3456.lm'),
    dic=os.path.join(model_path, '3456.dic')
)