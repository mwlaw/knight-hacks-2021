# Knight Hacks 2021

Detects audio from a microphone and lowers/mutes a user's volume if there is someone speaking.
