import speech_recognition as sr

def LowerVolume():
    print("Pretending to lower the volume")

r = sr.Recognizer()
mic = sr.Microphone()
name = "Marcus"
with mic as source:
    while True:
        print("New cycle")
        text = ""
        print("Listening...")
        r.adjust_for_ambient_noise(source)
        audio = r.listen(source)
        try:
            print("Interpreting...")
            text = r.recognize_google(audio)
        except:
            print("Error: No audio/incomprehensible noises")
        if text != "":
            texts = text.split()
            print(texts)
            if name in texts:
                LowerVolume()