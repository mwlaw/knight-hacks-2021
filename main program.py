import win32api
from win32con import VK_MEDIA_PLAY_PAUSE, KEYEVENTF_EXTENDEDKEY
from ctypes import cast, POINTER
from comtypes import CLSCTX_ALL
from pycaw.pycaw import AudioUtilities, IAudioEndpointVolume
devices = AudioUtilities.GetSpeakers()
interface = devices.Activate(IAudioEndpointVolume._iid_, CLSCTX_ALL, None)
volume = cast(interface, POINTER(IAudioEndpointVolume))
import webbrowser
import speech_recognition as sr

# Text to speech initalization
import pyttsx3

engine1=pyttsx3.init()
engine1.setProperty('rate', 125)
engine2=pyttsx3.init()
engine2.setProperty('rate', 150)
# engine.say("I will say this.")
# engine.runAndWait()

# variables for option select
mute_opt = False
pause_opt = False
started = False
vol_perc = -1

# startup program: initialize necessary things

# prompt for user name 
print("Welcome to the audio program.")
engine2.say("Hello, How may I be at your assistance?")
engine2.runAndWait()
print("Please enter your name: ")
name = input().lower()

# loop until valid options are initialized
while (not started):
    engine2.say('Please configure the program.')
    engine2.runAndWait()
    print("What action should be performed to the audio when your name is called? Options are decrease volume (d), mute (m), pause (p)")
    option = input()
    # initialize the option
    started = True
    if option == 'd':
        while vol_perc > 100 or vol_perc < 0:
            print("How much percent do you want to lower the volume by?")
            vol_perc = int(input())
            if vol_perc > 100 or vol_perc < 0:
                print("Invalid percentage.")
            else:
                started = True
    elif option == 'm':
        mute_opt = True
        started = True
    elif option == 'p':
        pause_opt = True
        started = True
    else:
        print("Invalid option.")
        # loop back to option select

reg_vol = volume.GetMasterVolumeLevelScalar()
reg_mute = volume.GetMute()

# microphone section
def LowerVolume():

    # record original program values
    reg_vol = volume.GetMasterVolumeLevelScalar()
    reg_mute = volume.GetMute()
    # perform specified action
    if vol_perc <= 100 and vol_perc >= 0:
        print("Lowering the volume")
        lowered_volume = reg_vol * (1.0 - (vol_perc / 100.0))
        volume.SetMasterVolumeLevelScalar(lowered_volume, None)
    elif mute_opt:
        if not reg_mute:
            print("Muting")
            volume.SetMute(True, None)
        else:
            print("Already Muted")
    elif pause_opt:
        print("Pressing Pause/Play")
        win32api.keybd_event(VK_MEDIA_PLAY_PAUSE, 0, KEYEVENTF_EXTENDEDKEY, 0)
    else:
        print("Lower Options not configured properly, please restart program.")
    
    engine1.say("Someone is talking to you")
    engine1.runAndWait()

def ResumeVolume():
    if vol_perc <= 100 and vol_perc >= 0:
        if abs(volume.GetMasterVolumeLevelScalar() - reg_vol * (1 - (vol_perc / 100.0))) < 0.01:
            print("Resuming audio")
            volume.SetMasterVolumeLevelScalar(reg_vol, None)
        else:
            print("Volume was changed, cancelled auto resume.")
    elif mute_opt:
        if reg_mute:
            print("Unmuting")
            volume.SetMute(False, None)
        else:
            print("Already unmuted")
    elif pause_opt:
        print("Pressing Pause/Play")
        win32api.keybd_event(VK_MEDIA_PLAY_PAUSE, 0, KEYEVENTF_EXTENDEDKEY, 0)
    else:
        print("Resume Options not configured properly, please restart program.")

def End():
    engine2.say("Goodbye")
    engine2.runAndWait()
    exit()

def OpenSpotify():
    engine2.say("opening Spotify.")
    engine2.runAndWait()
    webbrowser.open_new_tab('https://open.spotify.com/?_ga=2.81230487.1850307107.1636835516-192670859.1635115228')


def VoiceCommand(text_array):
    texts = text_array
    if name in texts:
        LowerVolume()
    if "resume" in texts:
        ResumeVolume()
    if 'open' and 'spotify' in texts:
        OpenSpotify()
    if "exit" in texts:
        End()
    



r = sr.Recognizer()
mic = sr.Microphone()
with mic as source:
    while True:
        print("New cycle")
        text = ""
        print("Listening...")
        r.adjust_for_ambient_noise(source)
        audio = r.listen(source)
        try:
            print("Interpreting...")
            text = r.recognize_google(audio)
        except:
            print("Error: No audio/incomprehensible noises")
        if text != "":
            texts = text.split()
            texts = [x.lower() for x in texts ]
            print(text)
            VoiceCommand(texts)
        else:
            ResumeVolume()