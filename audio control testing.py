from ctypes import cast, POINTER
from comtypes import CLSCTX_ALL
from pycaw.pycaw import AudioUtilities, IAudioEndpointVolume
devices = AudioUtilities.GetSpeakers()
interface = devices.Activate(
    IAudioEndpointVolume._iid_, CLSCTX_ALL, None)
volume = cast(interface, POINTER(IAudioEndpointVolume))
mastervolume = 0.0
volume.GetMute()
mastervolume = volume.GetMasterVolumeLevelScalar()
print(mastervolume)
range = volume.GetVolumeRange()
print(range)
volume.SetMasterVolumeLevelScalar(mastervolume*0.8, None)
mastervolume = volume.GetMasterVolumeLevelScalar()
print(mastervolume)
volume.SetMute(True, None)
mastervolume = volume.GetMasterVolumeLevelScalar()
print(mastervolume)

#monitor loop
#if conditions are met
reg_vol = volume.GetMasterVolumeLevelScalar()
volume.SetMasterVolumeLevelScalar(reg_vol*0.8, None)
#if mute enabled
#mute instead
#if 5 seconds and reg_vol*0.8 = cur_vol
volume.SetMasterVolumeLevelScalar(reg_vol, None)
#if mute is enabled, then unmute